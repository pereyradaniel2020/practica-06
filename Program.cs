﻿using System;

namespace practica_06
{
//Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad (definir las propiedades para poder acceder a dichos atributos)". Definir como responsabilidad un método para mostrar ó imprimir. Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y llamar a sus métodos y propiedades.
    class Persona
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Cedula { get; set; }
        public int Edad { get; set; }

        public  void  Imprimir ()
        {
            Console.WriteLine ( "  Cedula : "  +  Cedula );
            Console.WriteLine ( "  Nombre : "  +  Nombre );
            Console.WriteLine ( "  Apellido : "  +  Apellido );
            Console.WriteLine ( "  Edad : "  +  Edad );
        }

    }

    class Profesor:Persona
    {
        public int Sueldo { get; set; }

        public  void  Imprimir1 ()
        {
       
            Console.WriteLine ( "  sueldo : "  +  Sueldo );
         
        }
    }
    

   
    class Cliente

    {
        static void Main(string[] args)
        {
          Persona P1 = new Persona();
            P1.Nombre = "Daniel";
            P1.Apellido = "Pereyra";
            P1.Cedula = 12345678;
            P1.Edad = 19;
        Profesor Pr1 = new Profesor();
            Pr1.Sueldo = 20000;

            Console.WriteLine("Datos personas");
            P1.Imprimir();
            Pr1.Imprimir1();
            Console.ReadKey();

        }
    }
}