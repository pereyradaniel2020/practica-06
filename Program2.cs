using System;

namespace practica_06
{
    //Crear una clase Contacto. Esta clase deberá tener los atributos "nombre, apellidos, telefono y direccion". También deberá tener un método "SetContacto", de tipo void y con los parámetros string, que permita cambiar el valor de los atributos. También tendrá un método "Saludar", que escribirá en pantalla "Hola, soy " seguido de sus datos. Crear también una clase llamada ProbarContacto. Esta clase deberá contener sólo la función Main, que creará dos objetos de tipo Contacto, les asignará los datos del contacto y les pedirá que saluden.
    class Contacto
    {
        public string nombre;
        public string apellidos;
        public long telefono;
        public string direccion;

          public void SetContacto(string nombre, string apellidos, long telefono, string direccion)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.telefono = telefono;
            this.direccion = direccion;
        }
        public void Saludar ()
        {
            Console.WriteLine(" Hola soy "  +  nombre  +  "  "  +  apellidos  +  " , mi telefono es "  +  telefono  +  " y vivo es "  +  direccion);
        }
    }
    
        class ProbarContador

    {
        static void Main(string[] args)
        {
            Contacto c1 = new Contacto();
            c1.SetContacto ("Daniel", "Pereyra", 8298393288, "Pantoja");
            c1.Saludar();

            Contacto c2 = new Contacto();
            c2.SetContacto ("Alex", "Beltran", 8095431234, "Piantini");
            c2.Saludar();
        }
    }
}
